// Who doesn't love Stack overflow
var openPorts = (function () {
  var index = 0;
  var ports = {};
  var op = {
    getPorts: function () {
      var result = {};
      for (var id in ports) {
        result[id] = ports[id];
      }
      return result;
    },
    getPortsArray: function () {
      var result = [];
      for (var id in ports) {
        result.push(ports[id]);
      }
      return result;
    },
    get: function (id) {
      return ports[id];
    },
    add: function (port) {
      var id = index;

      ports[id] = port;
      port.onDisconnect.addListener(function () {
        op.remove(id);
      });

      index++;
      return id;
    },
    remove: function (id) {
      delete ports[id];
    },
    messageAll: function (message) {
      for (var id in ports) {
        ports[id].postMessage(message);
      }
    }
  };
  return op;
})();

const getLowColor = (color) => {
  //return Color(color).fade(0.8);
  return null;
}

const runtime = chrome.runtime != null ? chrome.runtime : browser.runtime;

chrome.storage.sync.get(["themeColor", "googleSans"], (items) => {
  if (!items || !items.themeColor) return;
  if (!items) return;
  var message = { msg: "update-theme" };

  if (items.themeColor) {
    themeColor = items.themeColor;
    message.lowColor = getLowColor(themeColor);
    message.color = themeColor;
  } else {
    message.color = themeColor;
  }

  if (typeof (items.googleSans) == "boolean") {
    message.googleSans = items.googleSans;
    googleSans = items.googleSans;
  }

  openPorts.messageAll(message);
});

runtime.onConnect.addListener((port) => {
  console.log("Connected to a new internal port");
  openPorts.add(port);
  port.onMessage.addListener((response) => {
    console.log("Got message: " + response.msg);
    switch (response.msg) {
      case "get-theme":
        port.postMessage({ msg: "update-theme", color: themeColor, googleSans: googleSans });
        break;
      case "update-theme":
        if (response.color) {
          themeColor = response.color;
        }
        if (typeof (googleSans) == "boolean") {
          googleSans = response.googleSans;
        }
        uploadTheme();
        openPorts.messageAll({
          msg: "update-theme",
          color: themeColor,
          lowColor: getLowColor(themeColor),
          googleSans
        });
        break;
      case "reset-theme":
        themeColor = defaultThemeColor;
        googleSans = defaultGoogleSansEnabled;
        uploadTheme();
        openPorts.messageAll({
          msg: "update-theme",
          color: themeColor,
          lowColor: getLowColor(themeColor),
          googleSans
        });
        break;
    }
  });
});

const uploadTheme = () => {
  chrome.storage.sync.set({ themeColor, googleSans }, () => {
    console.log(`Cloud theme upload complete, ${themeColor} ${googleSans}`);
  });
};

let themeColor = "#fd2448";
let googleSans = false;
const defaultThemeColor = "#fd2448";
const defaultGoogleSansEnabled = false;