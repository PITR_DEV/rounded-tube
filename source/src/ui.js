const extension = chrome != null ? chrome.runtime : browser.runtime;

var port;
let colorPicker;
let colorPickerContainer;
let isPickerVisible = false;

// let googleSansButton;
// let googleSansEnabled = false;

document.addEventListener(
  "DOMContentLoaded",
  () => {
    colorPickerContainer = document.getElementById("color-picker-container");
    colorPickerContainer.style.setProperty("display", "none");
    // googleSansButton = document.getElementById("theme-google-sans");
    // googleSansButton.addEventListener(
    //   "click",
    //   () => {
    //     toggleGoogleSans();
    //   },
    //   false
    // );

    const themeColorButon = document.getElementById("theme-color-button");
    themeColorButon.addEventListener(
      "click",
      () => {
        toggleColorPicker();
      },
      false
    );

    const colorInput = document.getElementById("color-input");
    colorInput.addEventListener("change", (event) => {
      updateColorPicker(event.target.value);
      setTheme(event.target.value, false);
    });

    colorPicker = new iro.ColorPicker("#color-picker", {
      borderWidth: 3,
      borderColor: "white",
    });
    colorPicker.on("input:end", (color) => {
      setTheme(color.rgbString, false);
    });
    console.log("Load done. Connecting...");

    port = extension.connect({ name: "main" });
    port.postMessage({ msg: "get-theme" });

    port.onMessage.addListener((response) => {
      console.log("Got message: " + response.msg);

      if (response.msg == "update-theme") {
        document.getElementById("payload-modal").style.display = "none";
        if (response.color) {
          updateColorPicker(response.color);
          colorInput.value = response.color;
          document.documentElement.style.setProperty(
            "--theme-color",
            response.color
          );
        }
        // if (typeof response.googleSans == "boolean") {
        //   googleSansEnabled = response.googleSans;
        //   updateGoogleSansButton();
        // }
      }
    });
  },
  false
);

const toggleColorPicker = () => {
  isPickerVisible = !isPickerVisible;
  // googleSansButton.style.setProperty(
  //   "display",
  //   isPickerVisible ? "none" : "block"
  // );
  colorPickerContainer.style.setProperty(
    "display",
    isPickerVisible ? "block" : "none"
  );
  setWindowHeight(isPickerVisible ? "600px" : "200px");
};

// const toggleGoogleSans = () => {
//   googleSansEnabled = !googleSansEnabled;
//   updateGoogleSansButton();
//   setTheme(null, googleSansEnabled);
// };

// const updateGoogleSansButton = () => {
//   googleSansButton.innerHTML = `Google Sans: ${
//     googleSansEnabled ? "Enabled" : "Disabled"
//   }`;
// };

const setTheme = (color, googleSans) => {
  port.postMessage({
    msg: "update-theme",
    color,
    googleSans,
  });
};

const setWindowHeight = (height) => {
  document.documentElement.style.setProperty("--window-height", height);
};

const updateColorPicker = (color) => {
  if (color.substr(0, 1) == "#") {
    colorPicker.color.hexString = color;
  } else {
    colorPicker.color.rgbString = color;
  }
};

const resetThemeColor = () => {
  port.postMessage({
    msg: "reset-theme",
  });
};
