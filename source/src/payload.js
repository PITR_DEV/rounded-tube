// Chrome / Firefox Extension Runtime Polyfill
const extension =
  chrome != null
    ? chrome.runtime
    : browser == null
    ? null
    : browser.runtime;

let googleSansInjected = false;
let faviconElement;

const setThemeColor = (color) => {
  document.documentElement.style.setProperty("--theme-color", color);
};

const setGoogleSans = (state) => {
  if (window.location.hostname != "www.youtube.com") return;
  if (state) {
    if (!googleSansInjected) {
      googleSansInjected = true;
      document.head.innerHTML +=
        "<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Google+Sans:400,300,300italic,400italic,500,500italic,700,700italic' type='text/css'>";
    }
    document.documentElement.style.setProperty(
      "--ytd-video-primary-info-renderer-title-font-family",
      "Google Sans"
    );
    document.documentElement.style.setProperty("font-family", "Google Sans");
  }
  if (typeof state == "boolean" && !state && googleSansInjected) {
    document.documentElement.style.setProperty(
      "--ytd-video-primary-info-renderer-title-font-family",
      "Roboto, Arial, sans-serif"
    );
    document.documentElement.style.setProperty(
      "font-family",
      "Roboto, Arial, sans-serif"
    );
  }
};

(() => {
  var port = extension.connect({ name: "main" });
  port.postMessage({ msg: "get-theme" });
  port.onMessage.addListener(function (response) {
    if (response.msg == "update-theme") {
      // Theme Color
      if (response.color) setThemeColor(response.color);

      // Google Sans
      if (typeof response.googleSans == "boolean")
        setGoogleSans(response.googleSans);

      // Mono Favicon
      if (response.monoFavicon) {
        var favicon_link_html = document.createElement("link");
        favicon_link_html.rel = "icon";
        favicon_link_html.href = extension.getURL("assets/favicon.ico");
        favicon_link_html.type = "image/x-icon";
        faviconElement = document
          .getElementsByTagName("head")[0]
          .appendChild(favicon_link_html);
      } else {
      }
    }
  });
})();
