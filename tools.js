const fs = require("fs");
const path = require("path");

const deleteFolderRecursive = function(folderPath) {
  if (fs.existsSync(folderPath)) {
    fs.readdirSync(folderPath).forEach((file, index) => {
      const curPath = path.join(folderPath, file);
      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(folderPath);
  }
};

const copyFileSync = (source, target) => {
  var targetFile = target;

  if (fs.existsSync(target)) {
    if (fs.lstatSync(target).isDirectory()) {
      targetFile = path.join(target, path.basename(source));
    }
  }

  fs.writeFileSync(targetFile, fs.readFileSync(source));
};

const copyFolderRecursiveSync = (source, target) => {
  var files = [];

  var targetFolder = path.join(target, path.basename(source));
  if (!fs.existsSync(targetFolder)) {
    fs.mkdirSync(targetFolder);
  }

  if (fs.lstatSync(source).isDirectory()) {
    files = fs.readdirSync(source);
    files.forEach(function(file) {
      var curSource = path.join(source, file);
      if (fs.lstatSync(curSource).isDirectory()) {
        copyFolderRecursiveSync(curSource, targetFolder);
      } else {
        copyFileSync(curSource, targetFolder);
      }
    });
  }
};

const successExit = () => {
  console.log("---------------------------------------------");
  console.log("Success! This process will exit in 3 seconds");
  setTimeout(() => {
    process.exit();
  }, 3000);
};

const outputDir = path.join(__dirname, "output");
const sourceDir = path.join(__dirname, "source");

const generateForPlatform = platformName => {
  const platformOutputPath = path.join(
    outputDir,
    `rounded-tube-${platformName}`
  );
  console.log(`Removing ${platformOutputPath}`);
  deleteFolderRecursive(platformOutputPath);
  console.log(`Making a copy of the source ${sourceDir}`);
  copyFolderRecursiveSync(sourceDir, outputDir);
  console.log(`Generating the extension directory ${platformOutputPath}`);
  fs.renameSync(path.join(outputDir, "source"), platformOutputPath);
  console.log(
    `Moving platform specific files ${path.join(
      __dirname,
      "data",
      platformName,
      "manifest.json"
    )}`
  );
  copyFileSync(
    path.join(__dirname, "data", platformName, "manifest.json"),
    platformOutputPath
  );
};

let argumentProvided = false;
process.argv.forEach(arg => {
  switch (arg) {
    case "generate":
      if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);
      deleteFolderRecursive(path.join(outputDir, "source"));
      generateForPlatform("chrome");
      generateForPlatform("firefox");
      argumentProvided = true;
      successExit();
  }
});

if (!argumentProvided) console.error("No arguments provided!");
