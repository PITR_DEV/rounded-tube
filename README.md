# Rounded Tube
![Rounded Tube Logo](https://gitlab.com/PITR_/rounded-tube/-/raw/master/source/icon128.png)

[Chrome Link](https://chrome.google.com/webstore/detail/edeocnllmaooibmigmielinnjiihifkn/)

[Firefox Link](https://addons.mozilla.org/en-US/firefox/addon/rounded-tube/)
